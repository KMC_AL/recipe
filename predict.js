/*
  Purpose: Pass information to other helper functions after a user clicks 'Predict'
  Args:
    value - Actual filename or URL
    source - 'url' or 'file'
*/


 var list = "";
 var items =[];
 var images_display =[];
 var images_clarifai =[];
 var index = 0 ;
function predict_click(value, source) {
  // first grab current index
 

  if(source === "url") {
   

  images_display[index % 4]=value;
  images_clarifai[index % 4]=value;
    index++;
    for(i=0 ; i< images_display.length;i++){
     document.getElementById("img_preview" + i).src = images_display[i];
    }
    console.log(images_display);

    // Div Stuff
    createHiddenDivs("url", value);
  }

  else if(source === "file") {
    
    //var preview = document.querySelector("#img_preview" );
    var file    = document.querySelector("input[type=file]").files[0];
    var reader  = new FileReader();

    // load local file picture
    reader.addEventListener("load", function () {
      var localBase64 = reader.result.split("base64,")[1];
         images_display[index % 4]="data:image/jpeg;base64,"+localBase64;
         images_clarifai[index % 4]=localBase64;
         index++;
         console.log("images_display:");
        console.log(images_display);
        console.log("images_clarifai");
        console.log(images_clarifai);
         for(i=0 ; i< images_display.length;i++){
     document.getElementById("img_preview" + i).src = images_display[i];
    }
      // Div Stuff
    //  createHiddenDivs("base64", localBase64);

    }, false);

    if (file) {
      reader.readAsDataURL(file);
    }
  }
}


function predict(){
   var modelID = getSelectedModel();
   var modelHeader = '<b><span style="font-size:14px">Choose items from the list:</span></b>';
   var conceptNames = "";
   var tagCount = 0;
   for (i = 0 ; i < images_clarifai.length ; i++){
    app.models.predict(modelID, images_clarifai[i]).then(
       function(response) {
       var tagArray;
       
       
       if(response.rawData.outputs[0].data.hasOwnProperty("concepts")) {
         
        tagArray = response.rawData.outputs[0].data.concepts;
       


        for (var other = 0; other < tagArray.length-10; other++) {
          conceptNames += '<li>' + tagArray[other].name + ': <i>' + tagArray[other].value + "</i><input type='checkbox' id="+(tagCount+other)+" name='ingredients' onchange='getSelectedItem()'  value="+tagArray[other].name +" /></li>"
          ;
         
        }
      console.log("in perdict");

        tagCount=tagCount+tagArray.length-10;
      }

      // Bad region request
  
        else {
          $('#concepts').html("<br/><br/><b>No Focus Regions Detected!</b>");
        }
      	return;
     

         
       }

      );
   } // for
   setTimeout(function(){
       console.log("timeout");
      console.log("after perdict");
      var columnCount = tagCount / 10;
      conceptNames = '<ul style="margin-right:20px; margin-top:20px; columns:' + columnCount + '; -webkit-columns:' + columnCount + '; -moz-columns:' + columnCount + ';">' + conceptNames;
      conceptNames += '</ul>';
      conceptNames = modelHeader + conceptNames;
      conceptNames += "<br>For more ingredients type them here:<input type=text id='more' name'more' /><br>"
      $('#concepts').html(conceptNames+"<br><button onclick='search()'>Search</button>");
      alert("after perdict :"+ tagCount);
   },5000); //delay is in milliseconds 

     
   
}

/*
  Purpose: Does a v2 prediction based on user input
  Args:
    value - Either {url : urlValue} or { base64 : base64Value }
*/
function doPredict(value) {

  var modelID = getSelectedModel();

  app.models.predict(modelID, value).then(

    function(response) {
      console.log(response);
      var conceptNames = "";
      var tagArray, regionArray;
      var tagCount = 0;
      var modelName = response.rawData.outputs[0].model.name;
      var modelNameShort = modelName.split("-")[0];
      var modelHeader = '<b><span style="font-size:14px">' + capitalize(modelNameShort) + ' Model</span></b>';

      // Check for regions models first
      if(response.rawData.outputs[0].data.hasOwnProperty("regions")) {
        regionArray = response.rawData.outputs[0].data.regions;
  
        // Regions are found, so iterate through all of them
        for(var i = 0; i < regionArray.length; i++) {
      	  conceptNames += "<b>Result " + (i+1) + "</b>";
         
      	  // Demographic has separate sub-arrays
      	  if(modelName == "demographics") {
      	    var ageArray = regionArray[i].data.face.age_appearance.concepts;
      	    var ethnicArray = regionArray[i].data.face.multicultural_appearance.concepts;
      	    var genderArray = regionArray[i].data.face.gender_appearance.concepts;

            // Age Header
      	    conceptNames += '<br/><b><span style="font-size:10px">Age Appearance</span></b>';

      	    // print top 5 ages
      	    for(var a = 0; a < 5; a++) {
      	      conceptNames += '<li>' + ageArray[a].name + ': <i>' + ageArray[a].value + '</i></li>';
            }

      	    // Ethnicity Header
      	    conceptNames += '<b><span style="font-size:10px">Multicultural Appearance</span></b>';

      	    // print top 3 ethnicities
      	    for(var e = 0; e < 3; e++) {
      	      conceptNames += '<li>' + ethnicArray[e].name + ': <i>' + ethnicArray[e].value + '</i></li>';
            }

      	    // Gender Header
      	    conceptNames += '<b><span style="font-size:10px">Gender Appearance</span></b>';

      	    // print gender
      	    conceptNames += '<li>' + genderArray[0].name + ': <i>' + genderArray[0].value + '</i></li>';
      	}

     
      	

    
      	// Focus
      	else if(modelName == "focus") {
      	  // Print total focus score and all regions with focus
      	  conceptNames += '<br/><b><span style="font-size:10px">Focus Region</span></b>';
      	  conceptNames += '<li>Top Row: <i>' + regionArray[i].region_info.bounding_box.top_row + '</i></li>';
      	  conceptNames += '<li>Left Column: <i>' + regionArray[i].region_info.bounding_box.left_col + '</i></li>';
      	  conceptNames += '<li>Bottom Row: <i>' + regionArray[i].region_info.bounding_box.bottom_row + '</i></li>';
      	  conceptNames += '<li>Right Column: <i>' + regionArray[i].region_info.bounding_box.right_col + '</i></li>';
          alert("conceptNames:"+conceptNames);
          alert("regionArray:"+ regionArray);
          if(i === regionArray.length - 1) {
      		  conceptNames += '<br><br><li><br><br>Overall Focus: <i>' + response.rawData.outputs[0].data.focus.value + '</i><br><br><br></li>';
      		}
      	}

      	tagCount+=10;
      }
     }

      // Color Model
      else if(modelName === "color") {
        alert("SDD");
      	conceptNames += '<b><span style="font-size:10px">Colors</span></b>';
        tagArray = response.rawData.outputs[0].data.colors;

        for (var col = 0; col < tagArray.length; col++) {
          conceptNames += '<li>' + tagArray[col].w3c.name + ': <i>' + tagArray[col].value + '</i></li>';
        }

        tagCount=tagArray.length;
      }

      // Generic tag response models
      else if(response.rawData.outputs[0].data.hasOwnProperty("concepts")) {
         
        tagArray = response.rawData.outputs[0].data.concepts;
        conceptNames +="  <form action=''/> ";


        for (var other = 0; other < tagArray.length-10; other++) {
          conceptNames += '<li>' + tagArray[other].name + ': <i>' + tagArray[other].value + "</i><input type='checkbox' id="+other+" name='ingredients' onchange=getSelectedItem()  value="+tagArray[other].name +" /></li>"
          ;
        }
        conceptNames +="</form>"

        tagCount=tagArray.length;
      }

      // Bad region request
      else {
      	if(modelName != "logo" && modelName != "focus") {
          $('#concepts').html("<br/><br/><b>No Faces Detected!</b>");
        }
      	else if(modelName == "logo") {
          $('#concepts').html("<br/><br/><b>No Logos Detected!</b>");
        }
        else {
          $('#concepts').html("<br/><br/><b>No Focus Regions Detected!</b>");
        }
      	return;
      }

      var columnCount = tagCount / 10;

      // Focus gets one more column
      if(modelName == "focus") {
      	columnCount += 1;
      }

      conceptNames = '<ul style="margin-right:20px; margin-top:20px; columns:' + columnCount + '; -webkit-columns:' + columnCount + '; -moz-columns:' + columnCount + ';">' + conceptNames;

      conceptNames += '</ul>';
      conceptNames = modelHeader + conceptNames;
     
      if(tagArray[0] != null){
         for (var other = 0; other < tagArray.length-15; other++) {
          list +=  tagArray[other].name;
          if(other != tagArray.length-16)
            list += ","
        }
        
      }


      $('#concepts').html(conceptNames+"<br><button onclick='search()'>Search</button>");
      
      
      document.getElementById("add-image-button").style.visibility = "visible";
    },
    function(err) {
      console.log(err);
    }
  );
}




function search(){
var html ="";
var typing;
if (document.getElementById("more").value != " "){
 typing =","+ document.getElementById("more").value;

  
   typing = typing.replace(" ", ",");
}

var url = "https://food2fork.com/api/search?key=10e968f1bc560367af178fd8b007240a&q="+items+typing;
url="https://cors-anywhere.herokuapp.com/https://food2fork.com/api/search?key=10e968f1bc560367af178fd8b007240a&q="+items+typing;
console.log(url);
// Create a request variable and assign a new XMLHttpRequest object to it.
var request = new XMLHttpRequest();

// Open a new connection, using the GET request on the URL endpoint
request.open('GET', url, true);

request.onload = function () {
  // Begin accessing JSON data here
  
   var data = JSON.parse(this.response);
   
  for (var i = 0; i < data.recipes.length; i++) {
  //  html+="<img src="+ data.recipes[i].image_url+" width=200 height=150 />";
    
   html+= " <a href="+data.recipes[i].f2f_url+"> <img src="+ data.recipes[i].image_url +" title='"+data.recipes[i].title+"&#013Publisher: "+data.recipes[i].publisher+"&#013Rank: "+data.recipes[i].social_rank+" &#013click to see more"+"' width=200 height=150 ></a>"
  }
  if(data.count ==0){
    html+= "<h1>Found no recipes, how about try with other ingredients?</h1>";
    
  }
  jQuery.support.cors = true;
   document.getElementById("recipes").innerHTML= html;
   console.log(data);
 }


// Send request
request.send();

}


/*
  Purpose: Return a back-end model id based on current user selection
  Returns:
    Back-end model id
*/
function getSelectedItem(){
  items=[];
  
  for(i = 0 ; images_clarifai.length*10 ; i++ ){
    if(  document.getElementById(i).checked == true)
    {
      items.push( document.getElementById(i).value);
    }
      
  }
  console.log(items);
   
}


function getSelectedModel() {
  var model = document.querySelector('input[name = "model"]:checked').value;

  if(model === "general") {
    return Clarifai.GENERAL_MODEL;
  }

  else if(model === "food") {
    return Clarifai.FOOD_MODEL;
  }



 
}


